Feature: Consultar CEP

  Scenario:  Consulta CEP valido

    Given que o usuário inseri um CEP válido
    When o serviço é consultado
    Then é retornado o CEP, logradouro, complemento, bairro, localidade, uf e ibge.


  Scenario:  Consulta CEP inexistente

    Given que o usuário inseri um CEP que não exista na base dos Correios
    When o serviço é consultado
    Then é retornada um atributo erro


  Scenario:  Consulta CEP com formato inválido

  Given que o usuário inseri um CEP com formato inválido
  When o serviço é consultado
  Then é retornado uma mensagem de erro