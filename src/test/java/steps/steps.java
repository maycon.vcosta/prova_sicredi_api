package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;

import static io.restassured.RestAssured.given;


public class steps {


    @Given("que o usuário inseri um CEP válido")
    public void que_o_usuario_inseri_um_cep_valido() {

        given()
                .when()
                .get("https://viacep.com.br/ws/52090003/json/");

    }

    @Given("que o usuário inseri um CEP que não exista na base dos Correios")
    public void que_o_usuario_inseri_um_cep_que_nao_exista_na_base_dos_correios() {

        given()
                .when()
                .get("https://viacep.com.br/ws/50000000/json/");

    }

    @Given("que o usuário inseri um CEP com formato inválido")
    public void que_o_usuario_inseri_um_cep_com_formato_invalido() {

        given()
                .when()
                .get("https://viacep.com.br/ws/52090-00/json/");

    }

    @When("o serviço é consultado")
    public void o_servico_e_consultado() {



    }

    @Then("é retornado o CEP, logradouro, complemento, bairro, localidade, uf e ibge.")
    public void e_retornado_o_cep_logradouro_complemento_bairro_localidade_uf_e_ibge() {

        given().then()


                .statusCode(200)
                .body("cep", Matchers.is("52090-003"))
                .body("logradouro", Matchers.is("Rua Nova Descoberta"))
                .body("complemento", Matchers.is("até 2388/2389"))
                .body("localidade", Matchers.is("Recife"))
                .body("uf", Matchers.is("PE"))
                .body("ibge", Matchers.is("2611606"))
        ;

    }

    @Then("é retornada um atributo erro")
    public void e_retornada_um_atributo_erro() {

        given()
                .then()
                .statusCode(200)
                .body("erro", Matchers.is(true))
        ;


    }

    @Then("é retornado uma mensagem de erro")
    public void e_retornado_uma_mensagem_de_erro() {
        given()
                .then()
                .statusCode(400)
                .body(Matchers.containsString("Erro 400"))
                .body(Matchers.containsString("Ops!"))
                .body(Matchers.containsString("Verifique a sua URL (Bad Request)"))

        ;

    }





}
